﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference 
        : BaseEntity
    {
        public Guid CustomerId;
        public Customer Customer;

        public Guid PreferenceId;
        public Preference Preference;
    }
}
