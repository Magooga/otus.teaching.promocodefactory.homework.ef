﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }

        public List<CustomerPreference> CustomerPreferences { get; set; }

        public Guid PromoCodeId { get; set; }
        public PromoCode PromoCode { get; set; }
    }
}