﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Data Source=helloapp.db");
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(sc => new { sc.PreferenceId, sc.CustomerId });

            //modelBuilder.Entity<Employee>()
            //    .HasOne(u => u.Role)
            //    .WithOne(c => c.Employee)
            //    .HasForeignKey<Role>(r => r.EmployeeId);

            modelBuilder.Entity<Employee>()
                .HasOne(u => u.Role)
                .WithOne(c => c.Employee)
                .HasForeignKey<Role>(c => c.EmployeeId);

            modelBuilder.Entity<PromoCode>()
                .HasOne(u => u.Preference)
                .WithOne(p => p.PromoCode)
                .HasForeignKey<Preference>(p => p.PromoCodeId);

            modelBuilder.Entity<Customer>()
                .HasMany(u => u.PromoCodes)
                .WithOne(c => c.Customer);

            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(50);

            modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(50);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(50);

            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(50);

            modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(50);


            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
        }

    }
}
